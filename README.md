# MagiWorld

Créer un **jeu de combat épique** entre Guerrier, Rôdeur et Mage ! 
Ce jeu est prévu pour **2 joueurs** et fonctionne dans **le terminal**.  

Pour plus détail, voici 
[le lien de l'énoncé](https://openclassrooms.com/fr/courses/4989236-apprenez-l-objet-avec-java/exercises/2660#/).  

## Planification

Pour réaliser cette activité je me suis organisé de la manière suivante :  

  - analyse et conception global
  - codage ... Pour chaque classe :  
    - "déclaration" ("structure/signature" - pas d'implémentation de la logique)
    - implémentation des tests
    - exécution des tests (résultat attendu : en échec - la classe n'est pas implémentée)
    - implémentation de la classe
    - exécution des tests (résultat attendu : succès)
    - rédaction de la JavaDoc
  - codage du programme principal (`Main`)
  - test et validation global

## Analyse et conception global

Diagramme UML du package `game` pour le projet MagiWorld :  

![Diagramme de classe du projet MagiWorld](classDiagramMagiWorld.png)  

Je n'ai pas indiqué les constructeurs, les accesseurs et les méthodes privées afin de gagner en clarté et facilité 
ainsi la compréhension.  

## Réalisation

Dans les faits j'ai réalisé l'activité de la manière suivante :  

  - pour chaque classe :  
    - analyse et conception détaillé (diagramme de classe complet, rédaction des algorithmes sur papier)
    - "déclaration" ("structure/signature" - pas d'implémentation de la logique)
    - implémentation des tests
    - exécution des tests (résultat attendu : en échec - la classe n'est pas implémentée)
    - implémentation de la classe
    - exécution des tests (résultat attendu : succès)
  - codage du programme principal (`Battle` et `Main`)
  - test et validation global (celà c'est fait manuellement avec le `Main` : fonctionnement nominal et les cas 
  d'erreur : mauvaises saisies utilisateur)
  - rédaction de la JavaDoc

## Conclusion

Initialement je voulais implémenter l'affichage sur la sortie standard uniquement dans la classe `Battle`, mais pour 
rester en accord avec l'énoncé de l'exercice j'ai du en mettre dans les classes filles de `Personage`. Si j'étais 
parvenu a bien séparer l'interface utilisateur (entrée / sortie) du reste du code, j'aurai pu utiliser la notion 
d'interface pour la classe `Battle`. Ainsi j'aurai pu implémenter cette interface et le code aurait pu être facilement 
réutilisé. Dans cette activité j'aurais implémenté une version CLI, mais il aurait été facilement possible d'implémenter
 d'autre version de cette interface (graphique, autre CLI, ...). Voici un exemple de diagramme de classe UML que nous 
aurions pu avoir pour l'interface `Battle`:  

![Diagramme de classe de l'interface Battle](classDiagramInterfaceBattle.png)  

Une activité très enrichissante qui ma permis :  
 
  - de mettre en application les connaissances acquises en Java
  - d'avoir une approche TDD
  - d'utiliser les bonnes pratiques

Pour mon prochain projet je veillerai a rédiger la JavaDoc durant la "déclaration" de mes classes.  

## Outils et technologies utilisés

  - **Draw.io:** dessin de diagrammes en ligne
  - **BitBucket :** forge logiciel (dépôt de code source, bugtracker, ...), voici 
  [le lien](https://bitbucket.org/openclassrooms/magiworld/src/master/)
  - **Git :** gestionnaire de version de code source décentralisé
  - **IntelliJ IDEA :** Environnement de Développement Intégré (IDE)
  - **Java :** langage de développement (`JDK11` et `JUnit 5.4.2`)
