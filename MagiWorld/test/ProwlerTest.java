import game.Prowler;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProwlerTest {

    @Test
    void givenLevel10_whenInstantiateProwler_thenGetValidProwler () {
        Prowler prowler = new Prowler(10);
        int[] expected = new int[]{10,50};
        int[] actual = new int[]{prowler.getLevel(), prowler.getLife()};
        assertArrayEquals(expected, actual);
    }

    @Test
    void givenLevel10AndStrength5_whenInstantiateProwler_thenGetValidProwler () {
        Prowler prowler = new Prowler(10, 5);
        int[] expected = new int[]{10,50,5};
        int[] actual = new int[]{prowler.getLevel(), prowler.getLife(), prowler.getStrength()};
        assertArrayEquals(expected, actual);
    }

    @Test
    void givenLevel10AndStrength5AndAgility3_whenInstantiateProwler_thenGetValidProwler () {
        Prowler prowler = new Prowler(10, 5, 3);
        int[] expected = new int[]{10,50,5,3};
        int[] actual = new int[]{prowler.getLevel(), prowler.getLife(), prowler.getStrength(), prowler.getAgility()};
        assertArrayEquals(expected, actual);
    }

    @Test
    void givenAllCharacteristics_whenInstantiateProwler_thenGetValidProwler () {
        Prowler prowler = new Prowler(10, 5, 3, 2);
        int[] expected = new int[]{10,50,5,3,2};
        int[] actual = new int[]{prowler.getLevel(), prowler.getLife(), prowler.getStrength(), prowler.getAgility(), prowler.getIntelligence()};
        assertArrayEquals(expected, actual);
    }

    @Test
    void givenLife50AndAgility5_whenAttack_thenReceives5Damage() {
        Prowler prowler = new Prowler(10, 3, 5, 2);
        prowler.attack(prowler);
        assertEquals(45, prowler.getLife());
    }

    @Test
    void givenLevel10AndAgility5_whenSpecialAttack_thenSelfGetBonusAgility5() {
        Prowler prowler = new Prowler(10, 3, 5, 2);
        prowler.specialAttack(prowler);
        assertEquals(5, prowler.getBonusAgility());
    }

    @Test
    void givenLevel11AndAgility5_whenSpecialAttack_thenSelfGetAgility5() {
        Prowler prowler = new Prowler(11, 3, 5, 3);
        prowler.specialAttack(prowler);
        assertEquals(5, prowler.getBonusAgility());
    }

    @Test
    void givenLife50AndAgility5_whenAttackAfterSpecialAttack_thenReceives10Damage() {
        Prowler prowler = new Prowler(10, 3, 5, 2);
        prowler.specialAttack(prowler);
        prowler.attack(prowler);
        assertEquals(40, prowler.getLife());
    }
}