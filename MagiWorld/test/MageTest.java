import game.Mage;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {

    @Test
    void givenLevel10_whenInstantiateMage_thenGetValidMage () {
        Mage mage = new Mage(10);
        int[] expected = new int[]{10,50};
        int[] actual = new int[]{mage.getLevel(), mage.getLife()};
        assertArrayEquals(expected, actual);
    }

    @Test
    void givenLevel10AndStrength5_whenInstantiateMage_thenGetValidMage () {
        Mage mage = new Mage(10, 5);
        int[] expected = new int[]{10,50,5};
        int[] actual = new int[]{mage.getLevel(), mage.getLife(), mage.getStrength()};
        assertArrayEquals(expected, actual);
    }

    @Test
    void givenLevel10AndStrength5AndAgility3_whenInstantiateMage_thenGetValidMage () {
        Mage mage = new Mage(10, 5, 3);
        int[] expected = new int[]{10,50,5,3};
        int[] actual = new int[]{mage.getLevel(), mage.getLife(), mage.getStrength(), mage.getAgility()};
        assertArrayEquals(expected, actual);
    }

    @Test
    void givenAllCharacteristics_whenInstantiateMage_thenGetValidMage () {
        Mage mage = new Mage(10, 5, 3, 2);
        int[] expected = new int[]{10,50,5,3,2};
        int[] actual = new int[]{mage.getLevel(), mage.getLife(), mage.getStrength(), mage.getAgility(), mage.getIntelligence()};
        assertArrayEquals(expected, actual);
    }

    @Test
    void givenLife50AndIntelligence5_whenAttack_thenReceives5Damage() {
        Mage mage = new Mage(10, 3, 2, 5);
        mage.attack(mage);
        assertEquals(45, mage.getLife());
    }

    @Test
    void givenIntelligence5_whenSpecialAttack_thenSelfGetBonusLife10() {
        Mage mage = new Mage(10, 3, 2, 5);
        mage.specialAttack(mage);
        assertEquals(10, mage.getBonusLife());
    }

    @Test
    void givenLife50AndIntelligence5_whenDamage3AfterSpecialAttack_thenSelfGetLife50AndBonusLife7() {
        Mage mage = new Mage(10, 3, 2, 5);
        mage.specialAttack(mage);
        mage.receiveDamage(3);
        int[] expected = new int[]{50,7};
        int[] actual = new int[]{mage.getLife(), mage.getBonusLife()};
        assertArrayEquals(expected, actual);
    }

    @Test
    void givenLife50AndIntelligence5_whenSpecialAttackAfterDamage3_thenSelfGetLife50AndBonusLife7() {
        Mage mage = new Mage(10, 3, 2, 5);
        mage.receiveDamage(3);
        mage.specialAttack(mage);
        int[] expected = new int[]{50,7};
        int[] actual = new int[]{mage.getLife(), mage.getBonusLife()};
        assertArrayEquals(expected, actual);
    }

    @Test
    void givenLife50AndIntelligence5_whenSpecialAttackAfterDamage10_thenSelfGetLife50AndBonusLife0() {
        Mage mage = new Mage(10, 3, 2, 5);
        mage.receiveDamage(10);
        mage.specialAttack(mage);
        int[] expected = new int[]{50,0};
        int[] actual = new int[]{mage.getLife(), mage.getBonusLife()};
        assertArrayEquals(expected, actual);
    }

    @Test
    void givenLife50AndIntelligence5_whenSpecialAttackAfterDamage12_thenSelfGetLife48AndBonusLife0() {
        Mage mage = new Mage(10, 3, 2, 5);
        mage.receiveDamage(12);
        mage.specialAttack(mage);
        int[] expected = new int[]{48,0};
        int[] actual = new int[]{mage.getLife(), mage.getBonusLife()};
        assertArrayEquals(expected, actual);
    }

    @Test
    void givenLife50AndIntelligence5_whenDamage12AfterSpecialAttackAfter_thenSelfGetLife48AndBonusLife0() {
        Mage mage = new Mage(10, 3, 2, 5);
        mage.specialAttack(mage);
        mage.receiveDamage(12);
        int[] expected = new int[]{48,0};
        int[] actual = new int[]{mage.getLife(), mage.getBonusLife()};
        assertArrayEquals(expected, actual);
    }
}