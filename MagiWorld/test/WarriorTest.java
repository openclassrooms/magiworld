import game.Warrior;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {

    @Test
    void givenLife50AndForce5_whenAttack_thenReceives5Damage() {
        Warrior warrior1 = new Warrior(10, 5, 3, 2);
        warrior1.attack(warrior1);
        assertEquals(45, warrior1.getLife());
    }

    @Test
    void givenLife50AndForce6_whenSpecialAttack_thenTargetReceives12DamageAndSelfReceive3Damage() {
        Warrior warrior1 = new Warrior(10, 6, 2, 2);
        warrior1.specialAttack(warrior1);
        assertEquals(35, warrior1.getLife());
    }

    @Test
    void givenLife50AndForce5_whenSpecialAttack_thenTargetReceives10DamageAndSelfReceive2Damage() {
        Warrior warrior1 = new Warrior(10, 5, 3, 2);
        warrior1.specialAttack(warrior1);
        assertEquals(38, warrior1.getLife());
    }
}