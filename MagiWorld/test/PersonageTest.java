import game.Personage;
import game.PersonageRuntimeException;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class PersonageTest {

    private Personage buildPersonage(int level) {
        return new Personage(level) {
            @Override
            public void attack(Personage enemy) {
            }
            @Override
            public void specialAttack(Personage enemy) {
            }
            @Override
            public void scream() {
                System.out.print("je fais un test ");
                System.out.println(this.getName() +
                        " niveau " + this.getLevel() +
                        " je possède " + this.getLife() +
                        " de vitalité, " + this.getStrength() +
                        " de force, " + this.getAgility() +
                        " d'agilité et " + this.getIntelligence() +
                        " d'intelligence !");
            }
        };
    }

    private Personage buildPersonage(int level, int strength, int agility, int intelligence) {
        return new Personage(level, strength, agility, intelligence) {
            @Override
            public void attack(Personage enemy) {

            }
            @Override
            public void specialAttack(Personage enemy) {

            }
            @Override
            public void scream() {
                System.out.print("je fais un test ");
                System.out.println(this.getName() +
                        " niveau " + this.getLevel() +
                        " je possède " + this.getLife() +
                        " de vitalité, " + this.getStrength() +
                        " de force, " + this.getAgility() +
                        " d'agilité et " + this.getIntelligence() +
                        " d'intelligence !");
            }
        };
    }

    @Test
    void givenInvalidLowerLevel_whenInstantiatePersonage_thenThrowsException() {
        assertThrows(PersonageRuntimeException.class, () -> buildPersonage(0));
    }

    @Test
    void givenInvalidHigherLevel_whenInstantiatePersonage_thenThrowsException() {
        assertThrows(PersonageRuntimeException.class, () -> buildPersonage(101));
    }

    @Test
    void givenValidParams_whenInstantiatePersonage_thenGetValidInstance() {
        Personage personage = buildPersonage(10, 5, 2, 3);
        assertEquals(10, (personage.getStrength() + personage.getAgility() + personage.getIntelligence()));
    }

    @Test
    void givenInvalidParams_whenInstantiatePersonage_thenThrowsExeption() {
        assertThrows(PersonageRuntimeException.class, () -> buildPersonage(5, 1, 1, 1));
        assertThrows(PersonageRuntimeException.class, () -> buildPersonage(5, 2, 2, 2));
    }

    @Test
    void givenLevel10_whenInstantiatePersonage_thenGetLife50() {
        assertEquals(50, buildPersonage(10).getLife());
    }

    @Test
    void givenLevel10_whenInstantiatePersonage_thenGetValidName() {
        Personage personage = buildPersonage(10);
        // get current instance count of game.Personage
        int nbInstance = Integer.parseInt(personage.getName().replaceAll("[^0-9]", ""));
        assertEquals(String.format("Joueur %d", (++nbInstance)), buildPersonage(10).getName());
    }

    @Test
    void given3_whenSettingStrength_thenGetStrength3() {
        Personage personage = buildPersonage(10);
        personage.setStrength(3);
        assertEquals(3, personage.getStrength());
    }

    @Test
    void given3_whenSettingAgility_thenGetAgility3() {
        Personage personage = buildPersonage(10);
        personage.setAgility(3);
        assertEquals(3, personage.getAgility());
    }

    @Test
    void given3_whenSettingIntelligence_thenGetIntelligence3() {
        Personage personage = buildPersonage(10);
        personage.setIntelligence(3);
        assertEquals(3, personage.getIntelligence());
    }

    @Test
    void givenInvalidStrength_whenSettingStrength_thenThrowsException() {
        Personage personage = buildPersonage(5);
        assertThrows(PersonageRuntimeException.class, () -> personage.setStrength(6), "invalid strength");
    }

    @Test
    void givenInvalidAgility_whenSettingAgility_thenThrowsException() {
        Personage personage = buildPersonage(3);
        assertThrows(PersonageRuntimeException.class, () -> personage.setAgility(4), "invalid agility");
    }

    @Test
    void givenInvalidIntelligence_whenSettingIntelligence_thenThrowsException() {
        Personage personage = buildPersonage(10);
        assertThrows(PersonageRuntimeException.class, () -> personage.setIntelligence(11), "invalid intelligence");
    }

    @Test
    void givenInvalidStrength_whenSettingPersonage_thenThrowsException() {
        Personage personage = buildPersonage(10);
        personage.setAgility(3);
        personage.setIntelligence(3);
        assertThrows(PersonageRuntimeException.class, () -> personage.setStrength(5), "invalid strength");
    }

    @Test
    void givenInvalidAgility_whenSettingPersonage_thenThrowsException() {
        Personage personage = buildPersonage(10);
        personage.setStrength(5);
        personage.setIntelligence(5);
        assertThrows(PersonageRuntimeException.class, () -> personage.setAgility(1), "invalid agility");
    }

    @Test
    void givenInvalidIntelligence_whenSettingPersonage_thenThrowsException() {
        Personage personage = buildPersonage(10);
        personage.setStrength(2);
        personage.setAgility(5);
        assertThrows(PersonageRuntimeException.class, () -> personage.setIntelligence(4), "invalid intelligence");
    }

    @Test
    void givenLevel10_whenPersonageReceiveDamage10_thenGetLife40() {
        Personage personage = buildPersonage(10);
        personage.receiveDamage(10);
        assertEquals(40, personage.getLife());
    }

    @Test
    void givenLevel10_whenPersonageReceiveDamage60_thenGetLife0() {
        Personage personage = buildPersonage(10);
        personage.receiveDamage(60);
        assertEquals(0, personage.getLife());
    }

    @Test
    void givenInstantiatePersonage_whenCleanThisInstance_thenDecrementTheInstanceCounter() {
        int nbInterface;
        try (Personage personage1 = buildPersonage(10)) {
            nbInterface = Personage.getNbPersonage();
        }
        assertEquals((--nbInterface), Personage.getNbPersonage());
    }
}