package game;

/**
 * Using by Personage class to generate a Runtime Exception.
 * When initializing an attribute of the Personage class, if it is outside the
 * valid range then this Runtime Exception is throws
 */
public class PersonageRuntimeException extends RuntimeException {
    public PersonageRuntimeException(String message) {
        super(message);
    }
}
