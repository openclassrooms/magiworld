package game;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * User interface to manage game, players and fight.
 * User interface is command line.
 * @author Mikael Flora
 * @version 1.0.0
 */
public class Battle {
    private List<Personage> players = new ArrayList<>();
    private Scanner sc = new Scanner(System.in);

    /**
     * Run battle.
     * User input to manage battle (personages and fight)
     */
    public void run() {
        try {
            this.beforeFight();
            this.fight();
            this.afterFight();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get className from command line interface.
     * @return className class name to use (expected: Warrior, Prowler or Mage)
     */
    private String readClassName() {
        String className = "";
        do {
            System.out.println("Veuillez choisir la classe de votre personnage (1: Guerrier, 2: Rôdeur, 3: Mage)");
            try {
                switch (sc.nextInt()) {
                    case 1:
                        className = "Warrior";
                        break;
                    case 2:
                        className = "Prowler";
                        break;
                    case 3:
                        className = "Mage";
                        break;
                    default:
                        System.out.println("Choix invalid !");
                        break;
                }
            } catch (InputMismatchException e) {
                sc.nextLine();
                System.out.println("Vous devez saisir un entier compris entre 1 et 3");
            }
        } while (className.equals(""));
        return className;
    }

    /**
     * Get personage characteristics from command line interface.
     * @return params list of characteristics (level, strength, agility and
     * intelligence)
     */
    private int[] readParams() {
        int[] params = new int[4];
        boolean error = false;

        do {
            try {
                error = false;
                System.out.println("Niveau du personnage ?");
                params[0] = sc.nextInt();
                System.out.println("Force du personnage ?");
                params[1] = sc.nextInt();
                System.out.println("Agilité du personnage ?");
                params[2] = sc.nextInt();
                System.out.println("Intelligence du personnage ?");
                params[3] = sc.nextInt();
            } catch (InputMismatchException e) {
                sc.nextLine();
                System.out.println("Vous devez saisir un entier !");
                error = true;
            }
        } while (error);
        return params;
    }

    /**
     * Instantiate personage from these class name and characteristics.
     * Use `readClassName()` and `readParams()` methods
     * @return personage (Warrior, Prowler or Mage)
     * @see Battle#readClassName()
     * @see Battle#readParams()
     */
    private Personage buildPersonage() {
        String className = this.readClassName();
        int[] params = this.readParams();

        switch (className) {
            case "Warrior":
                return new Warrior(params[0], params[1], params[2], params[3]);
            case "Prowler":
                return new Prowler(params[0], params[1], params[2], params[3]);
            case "Mage":
                return new Mage(params[0], params[1], params[2], params[3]);
            default:
                return null;
        }
    }

    /**
     * Display Welcome message and initialize the game.
     * @see Battle#buildPersonage()
     */
    private void beforeFight() {
        System.out.println("Bienvenue a MagiWorld !\n");

        do {
            try {
                this.players.add(this.buildPersonage());
            } catch (game.PersonageRuntimeException e) {
                System.out.println("Erreur : " + e.getMessage());
                System.out.println("Ressaisissez votre personnage.");
            }
        } while (this.players.size() != 2);
    }

    /**
     * Fighters clash.
     */
    private void fight () {
        System.out.println("Que le combat commence !");

        do {
            for (int i = 0; i < 2; i++) {
                if (this.players.get(i).getLife() == 0)
                    break;
                String message = this.players.get(i).getName() + " (" + this.players.get(i).getLife() + " Vitalité) veuillez choisir votre action (1: Attaque basique, 2: Attaque Spéciale)";
                System.out.println(message);
                switch (sc.nextInt()) {
                    case 1:
                        if (i == 0)
                            this.players.get(0).attack(this.players.get(1));
                        else
                            this.players.get(1).attack(this.players.get(0));
                        break;
                    case 2:
                        if (i == 0)
                            this.players.get(0).specialAttack(this.players.get(1));
                        else
                            this.players.get(1).specialAttack(this.players.get(0));
                        break;
                    default:
                        System.out.println("Choix invalide ! Dommage tu perds ton tour !");
                        break;
                }
            }
        } while (this.players.get(0).getLife() != 0 && this.players.get(1).getLife() != 0);
    }

    /**
     * Finalize game by displaying the loser.
     */
    private void afterFight() {
        if (this.players.get(0).getLife() == 0)
            System.out.println(this.players.get(0).getName() + " a perdu !");
        else
            System.out.println(this.players.get(1).getName() + " a perdu !");

        System.out.println("\nFin du combat !");
    }
}
