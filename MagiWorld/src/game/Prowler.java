package game;

/**
 * Prowler.
 * The main characteristics of the prowler are that he attacks with an archery
 * and he can concentrate to do more damage.
 * His strength is <b>agility</b>.
 * Prowler class inherits from Personage class
 * @author Mikael Flora
 * @see Personage
 */
public class Prowler extends Personage {
    private int bonusAgility = 0;

    /**
     * Instantiate a prowler.
     * @param level prowler level [1-100]. This param is used to calculate
     *              life {@code life = level x 5}
     * @throws PersonageRuntimeException invalid characteristic (outside
     * the range)
     * @see Personage#Personage(int) 
     */
    public Prowler(int level) throws PersonageRuntimeException {
        super(level);
    }

    /**
     * Instantiate a prowler.
     * @param level prowler level [1-100]. This param is used to calculate 
     *              life {@code life = level x 5}
     * @param strength prowler strength [0-100]
     * @see Personage#Personage(int, int) 
     */
    public Prowler(int level, int strength) {
        super(level, strength);
    }

    /**
     * Instantiate a prowler.
     * @param level prowler level [1-100]. This param is used to calculate 
     *              life {@code life = level x 5}
     * @param strength prowler strength [0-100]
     * @param agility prowler agility [0-100]
     * @see Personage#Personage(int, int, int) 
     */
    public Prowler(int level, int strength, int agility) {
        super(level, strength, agility);
    }

    /**
     * Instantiate a prowler.
     * @param level prowler level [1-100]. This param is used to calculate 
     *              life {@code life = level x 5}
     * @param strength prowler strength [0-100]
     * @param agility prowler agility [0-100]
     * @param intelligence prowler intelligence [0-100]
     * @see Personage#Personage(int, int, int) 
     */
    public Prowler(int level, int strength, int agility, int intelligence) {
        super(level, strength, agility, intelligence);
    }

    /**
     * Inflicts damage on an opposing personage with his archery.
     * {@code damage = agility + bonusAgility}
     * @param enemy opposing personage
     */
    @Override
    public void attack(Personage enemy) {
        System.out.println(this.getName() + " utilise Tir à l'Arc et inflige " +
                (this.getAgility() + bonusAgility) + " dommages.");
        enemy.receiveDamage(this.getAgility() + bonusAgility);
    }

    /**
     * Uses concentration to gain agility.
     * {@code bonusAgility += level / 2}
     * @param enemy opposing personage
     */
    @Override
    public void specialAttack(Personage enemy) {
        bonusAgility += getLevel() / 2;
        System.out.println(this.getName() + " utilise Concentration et gagne " +
                this.getAgility() + " d'agilité.");
    }

    /**
     * Display prowler scream on standard output.
     * @see Personage#scream()
     */
    @Override
    public void scream() {
        System.out.print("Chut je suis le Rôdeur ");
        super.scream();
    }

    /**
     * Get agility bonus.
     * The agility bonus is used during the attack and is obtained thanks to the
     * special attack (that is to said the concentration)
     * @return bonusAgility current agility bonus
     */
    public int getBonusAgility() {
        return this.bonusAgility;
    }
}
