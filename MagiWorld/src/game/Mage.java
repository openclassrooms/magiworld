package game;

/**
 * Mage.
 * The main characteristics of the mage are that he attacks with a fireball
 * and he can heal himself.
 * His strength is <b>intelligence</b>.
 * Mage class inherits from Personage class
 * @author Mikael Flora
 * @see Personage
 */
public class Mage extends Personage {
    private int bonusLife = 0;

    /**
     * Instantiate a mage.
     * @param level mage level [1-100]. This param is used to calculate life
     *              {@code life = level x 5}
     * @throws PersonageRuntimeException invalid characteristic (outside
     * the range)
     * @see Personage#Personage(int)
     */
    public Mage(int level) throws PersonageRuntimeException {
        super(level);
    }

    /**
     * Instantiate a mage.
     * @param level mage level [1-100]. This param is used to calculate life
     *              {@code life = level x 5}
     * @param strength mage strength [0-100]
     * @see Personage#Personage(int, int)
     */
    public Mage(int level, int strength) {
        super(level, strength);
    }

    /**
     * Instantiate a mage.
     * @param level mage level [1-100]. This param is used to calculate life
     *              {@code life = level x 5}
     * @param strength mage strength [0-100]
     * @param agility mage agility [0-100]
     * @see Personage#Personage(int, int, int)
     */
    public Mage(int level, int strength, int agility) {
        super(level, strength, agility);
    }

    /**
     * Instantiate a mage.
     * @param level mage level [1-100]. This param is used to calculate life
     *              {@code life = level x 5}
     * @param strength mage strength [0-100]
     * @param agility mage agility [0-100]
     * @param intelligence mage intelligence [0-100]
     * @see Personage#Personage(int, int, int, int)
     */
    public Mage(int level, int strength, int agility, int intelligence) {
        super(level, strength, agility, intelligence);
    }

    /**
     * Inflicts damage on an opposing personage with his fireball.
     * {@code damage = intelligence}
     * @param enemy opposing personage
     */
    @Override
    public void attack(Personage enemy) {
        System.out.println(this.getName() + " utilise Boule de Feu et inflige " +
                this.getIntelligence() + " dommages.");
        enemy.receiveDamage(this.getIntelligence());
    }

    /**
     * Uses magic care to gain life.
     * {@code life += intelligence x 2}
     * @param enemy opposing personage
     */
    @Override
    public void specialAttack(Personage enemy) {
        int bonusLife = this.getIntelligence() * 2;
        int maxLife = this.getLevel() * 5;
        int currentLife = this.getLife();

        System.out.println(this.getName() + " utilise Soin et gagne " +
                bonusLife + " en vitalité.");

        if (currentLife == maxLife) {
            this.bonusLife += bonusLife;
        }
        else {
            int deltaLife = maxLife - currentLife;
            if (deltaLife >= bonusLife) {
                this.addLife(bonusLife);
            }
            else {
                this.addLife(deltaLife);
                this.bonusLife += (bonusLife - deltaLife);
            }
        }
    }

    /**
     * Display mage scream on standard output.
     * @see Personage#scream() 
     */
    @Override
    public void scream() {
        System.out.print("Abracadabra je suis le Mage ");
        super.scream();
    }

    /**
     * Decreases life of the number of damage received.
     * @param nbDamage number of damage
     * @see Personage#receiveDamage(int) 
     */
    @Override
    public void receiveDamage(int nbDamage) {
        if (this.bonusLife == 0) {
            super.receiveDamage(nbDamage);
        }
        else if (this.bonusLife < nbDamage) {
            super.receiveDamage(nbDamage - this.bonusLife);
            this.bonusLife = 0;
        }
        else {
            System.out.println(this.getName() + " perd " +
                    nbDamage + " points de vie");
            this.bonusLife -= nbDamage;
        }
    }

    /**
     * Get life bonus.
     * The life bonus is obtained thanks to the special attack (that is to
     * said magic care)
     * @return bonusLife current life bonus
     */
    public int getBonusLife() {
        return this.bonusLife;
    }
}
