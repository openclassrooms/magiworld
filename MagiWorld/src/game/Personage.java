package game;

/**
 * Personage is base class used by Prowler, Mage and Warrior classes.
 * It defines the common characteristics of the personages
 * @author Mikael Flora
 * @see Prowler
 * @see Mage
 * @see Warrior
 */
public abstract class Personage implements AutoCloseable {
    private static int nbPersonage = 0;

    private String name;
    private int level;
    private int life;
    private int strength;
    private int agility;
    private int intelligence;

    /**
     * Instantiate a personage.
     * @param level personage level [1-100]. This param is used to calculate
     *              life {@code life = level x 5}
     * @throws PersonageRuntimeException invalid characteristic (outside the
     * range)
     */
    public Personage(int level) {
        Personage.nbPersonage++;

        if (level <= 0 || level > 100) throw new PersonageRuntimeException("invalid level");

        this.name = "Joueur " + Personage.nbPersonage;
        this.level = level;
        this.life = this.level * 5;
        this.strength = 0;
        this.agility = 0;
        this.intelligence = 0;
    }

    /**
     * Instantiate a personage.
     * @param level personage level [1-100]. This param is used to calculate
     *              life {@code life = level x 5}
     * @param strength personage strength [0-100]
     * @see Personage#Personage(int)
     */
    public Personage(int level, int strength) {
        this(level);
        this.setStrength(strength);
    }

    /**
     * Instantiate a personage.
     * @param level personage level [1-100]. This param is used to calculate
     *              life {@code life = level x 5}
     * @param strength personage strength [0-100]
     * @param agility personage agility [0-100]
     * @see Personage#Personage(int, int) 
     */
    public Personage(int level, int strength, int agility) {
        this(level, strength);
        this.setAgility(agility);
    }

    /**
     * Instantiate a personage.
     * @param level personage level [1-100]. This param is used to calculate
     *              life {@code life = level x 5}
     * @param strength personage strength [0-100]
     * @param agility personage agility [0-100]
     * @param intelligence personage intelligence [0-100]
     * @see Personage#Personage(int, int, int) 
     */
    public Personage(int level, int strength, int agility, int intelligence) {
        this(level, strength, agility);
        this.setIntelligence(intelligence);
        if (level != strength + agility + intelligence)
            throw new PersonageRuntimeException("Level != Strength + Agility + Intelligence");
        this.scream();
    }

    /**
     * Decrement the personage counter.
     * @see AutoCloseable#close()
     */
    @Override
    public void close() {
        Personage.nbPersonage--;
    }

    /**
     * Get the number of personages instantiated.
     * @return number of personages
     */
    public static int getNbPersonage() {
        return Personage.nbPersonage;
    }

    public String getName() {
        return name;
    }

    public int getLife() {
        return life;
    }

    public int getLevel() {
        return level;
    }

    public int getStrength() {
        return strength;
    }

    public int getAgility() {
        return agility;
    }

    public int getIntelligence() {
        return intelligence;
    }

    /**
     * Set personage strength
     * @param strength personage strength [0-100]
     */
    public void setStrength(int strength) {
        if (strength < 0 || strength >  (level - (agility + intelligence)))
            throw new PersonageRuntimeException("invalid strength");
        this.strength = strength;
    }

    /**
     * Set personage agility
     * @param agility personage agility [0-100]
     */
    public void setAgility(int agility) {
        if (agility < 0 || agility >  (level - (strength + intelligence)))
            throw new PersonageRuntimeException("invalid agility");
        this.agility = agility;
    }

    /**
     * Set personage intelligence
     * @param intelligence personage intelligence [0-100]
     */
    public void setIntelligence(int intelligence) {
        if (intelligence < 0 || intelligence >  (level - (strength + agility)))
            throw new PersonageRuntimeException("invalid intelligence");
        this.intelligence = intelligence;
    }

    /**
     * Display personage scream on standard output.
     * This scream gives level, life, strength, agility and intelligence of
     * the personage
     */
    public void scream() {
        System.out.println(this.getName() +
                " niveau " + this.getLevel() +
                " je possède " + this.getLife() +
                " de vitalité, " + this.getStrength() +
                " de force, " + this.getAgility() +
                " d'agilité et " + this.getIntelligence() +
                " d'intelligence !");
    }

    /**
     * Decreases life of the number of damage received.
     * @param nbDamage number of damage
     */
    public void receiveDamage(int nbDamage) {
        System.out.println(this.getName() + " perd " +
                nbDamage + " points de vie");
        life -= nbDamage;
        if (life <= 0) {
            life = 0;
            System.out.println(this.getName() + " est mort");
        }
    }

    /**
     * Add life to the personage.
     * Life can not exceed the maximum allowed, that is to said
     * {@code maxLife = level x 5}
     * @param life number of life
     */
    protected void addLife(int life) {
        if ((this.life + life) > (this.level * 5))
            this.life = this.level * 5;
        else if (life > 0)
            this.life += life;
    }

    /**
     * Inflicts damage on an opposing personage.
     * @param enemy opposing personage
     */
    public abstract void attack(Personage enemy);

    /**
     * Launches a special attack or spell.
     * This attack or spell can inflict damage or increase an ability about
     * yourself or an opponent
     * @param enemy opposing personage
     */
    public abstract void specialAttack(Personage enemy);
}
