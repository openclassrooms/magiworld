package game;

/**
 * Warrior.
 * The main characteristics of the warrior are that he attacks with a sword
 * and he give a rage to do more damage but in this case he loses life points.
 * His strength is <b>strength</b>.
 * Warrior class inherits from Personage class
 * @author Mikael Flora
 * @see Personage
 */
public class Warrior extends Personage {

    /**
     * Instantiate a warrior.
     * @param level warrior level [1-100]. This param is used to calculate
     *              life {@code life = level x 5}
     * @throws PersonageRuntimeException invalid characteristic (outside
     * the range)
     * @see Personage#Personage(int) 
     */
    public Warrior(int level) throws PersonageRuntimeException {
        super(level);
    }

    /**
     * Instantiate a warrior.
     * @param level warrior level [1-100]. This param is used to calculate
     *              life {@code life = level x 5}
     * @param strength warrior strength [0-100]
     * @see Personage#Personage(int, int) 
     */
    public Warrior(int level, int strength) {
        super(level, strength);
    }

    /**
     * Instantiate a warrior.
     * @param level warrior level [1-100]. This param is used to calculate
     *              life {@code life = level x 5}
     * @param strength warrior strength [0-100]
     * @param agility warrior agility [0-100]
     * @see Personage#Personage(int, int, int) 
     */
    public Warrior(int level, int strength, int agility) {
        super(level, strength, agility);
    }

    /**
     * Instantiate a warrior.
     * @param level warrior level [1-100]. This param is used to calculate
     *              life {@code life = level x 5}
     * @param strength warrior strength [0-100]
     * @param agility warrior agility [0-100]
     * @param intelligence warrior intelligence [0-100]
     * @see Personage#Personage(int, int, int, int) 
     */
    public Warrior(int level, int strength, int agility, int intelligence) {
        super(level, strength, agility, intelligence);
    }

    /**
     * Inflicts damage on an opposing personage with his sword.
     * {@code damage = strength}
     * @param enemy opposing personage
     */
    @Override
    public void attack(Personage enemy) {
        System.out.println(this.getName() + " utilise Coup d'Epée et inflige " +
                this.getStrength() + " dommages.");
        enemy.receiveDamage(this.getStrength());
    }

    /**
     * Inflicts damage on an opposing personage with his sword and rage.
     * {@code damage = strength x 2} but he loses life points
     * {@code life -= force / 2}
     * @param enemy opposing personage
     */
    @Override
    public void specialAttack(Personage enemy) {
        System.out.println(this.getName() + " utilise Coup de Rage et inflige " +
                (this.getStrength() * 2) + " dommages.");
        enemy.receiveDamage(this.getStrength() * 2);
        this.receiveDamage(this.getStrength() / 2);
    }

    /**
     * Display warrior scream on standard output.
     * @see Personage#scream()
     */
    @Override
    public void scream() {
        System.out.print("Woarg je suis le Guerrier ");
        super.scream();
    }
}
